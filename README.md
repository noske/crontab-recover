# CRONTAB RECOVER

if you send "crontab -r" and not have backup ... as me ...


## INFO

parse syslog file and generate the crontab records

## USAGE
basic - user(root), output to shell:
```bash
crontab_recover.py -i <inputfile>
```
advance:
```bash
crontab_recover.py -i <inputfile> -o <outputfile> -u <username>
```
### inputfile
required, file with syslog data

### outputfile
output result to this file

### username
if not insered, username is "root"