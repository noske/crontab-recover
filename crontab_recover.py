#!/usr/bin/python

from datetime import datetime
from itertools import groupby, izip
import re
import os
import sys
import getopt


def main(argv):
    global InputFile
    global OutputFile
    global User
    InputFile = None
    OutputFile = None
    User = None

    """ OPTS
    """
    try:
        opts, _ = getopt.getopt(argv, "hi:o:u:", ["ifile=", "ofile=", "user="])
    except getopt.GetoptError:
        print("crontab_recover.py -i <inputfile> -o <outputfile> -u <username>")
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print("crontab_recover.py -i <inputfile> -o <outputfile> -u <username>")
            sys.exit()
        elif opt in ("-i", "--ifile"):
            InputFile = arg
        elif opt in ("-o", "--ofile"):
            OutputFile = arg
        elif opt in ("-u", "--user"):
            User = arg

    if not InputFile:
        print 'crontab_recover.py -i <inputfile> -o <outputfile> -u <username>'
        sys.exit(2)

    if not User:
        User = "root"


class CommandLine():
    datetime = ""
    command = ""

    def __init__(self, datetime, command):
        self.datetime = datetime
        self.command = command

    def __repr__(self):
        return self.command


class CommandItem():
    command = ""
    first_time = None
    last_time = None
    runs = []
    intervals = []

    def __init__(self, command, first_time, last_time, runs, intervals):
        self.command = command
        self.first_time = first_time
        self.last_time = last_time
        self.runs = runs

        if intervals:
            self.intervals = intervals
        else:
            list_intervals = []
            last_run = None
            for run in runs:
                if last_run:
                    t = run - last_run
                    list_intervals.append(t)
                    last_run = run
                else:
                    last_run = run
                    continue
                self.intervals = list_intervals

    def get_interval(self):
        """ return average interval
            or None (if command will by running only once) (int)
        """
        if len(self.runs) > 1:
            total = sum(
                (next - last).seconds + (next - last).days * 86400 for next,
                last in izip(self.runs[1:], self.runs)
            )
            return (total / (len(self.runs) - 1)) / 60
        else:
            return None


def get_date(s_string):
    """ retunt datetime from string or None (datetime)
    """
    result = None
    try:
        result = datetime.strptime(s_string[:15], '%b %d %H:%M:%S')
    except ValueError:
        pass
    return result


def get_command(s_string):
    """ return string with command or None (string)
    """
    f_string = "%s%s%s" % (re.escape('CMD ('), "(.*)", re.escape(')'))
    reg_string = re.search(f_string, s_string)
    if reg_string:
        return reg_string.group(1)
    else:
        return None


def get_all_commands(file="/var/log/syslog", user="root"):
    """ return list of commands ([]CommandLine)
    """
    result = []

    f = os.popen("grep 'CRON.*({})' {}".format(user, file))
    for line in f.readlines():
        result.append(
            CommandLine(
                get_date(line),
                get_command(line)
            )
        )
    result.sort(key=lambda x: x.datetime, reverse=False)

    return result


def get_grouped_commands(commands_list):
    """ return grouped list ([]CommandLine)
    """
    result = {}

    c = groupby(commands_list, key=lambda x: x.command)
    for k, v in c:
        result[k] = list(v)

    return result


def get_command_item(command, commands_list):
    """ return object CommandItem with list of run command (CommandItem)
    """
    runs = []
    result = None
    records = filter(lambda x: command in x.command, commands_list)
    records.sort(key=lambda x: x.datetime, reverse=False)

    if records:
        first_time = records[0].datetime
        last_time = records[-1].datetime

        for record in records:
            runs.append(record.datetime)

        result = CommandItem(
            command=command,
            first_time=first_time,
            last_time=last_time,
            runs=runs,
            intervals=[]
        )

    return result


def get_command_timming(command):
    """ return timming strings in list (list)
    """
    timming_string = ["*", "*", "*", "*", "*"]

    if command.get_interval() < 60 and len(command.runs) > 1:
        timming_string[0] = "*/%s" % command.get_interval()
    elif command.get_interval() == 60 or len(command.runs) == 1:
        timming_string[1] = command.first_time.hour
        timming_string[0] = command.first_time.minute

    return timming_string


def info_messages(commands_list):
    """ return list of info lines (list)
    """
    text_lines = []
    text_lines.append(u"############################")
    text_lines.append(u"##    Crontab Recovery    ##")
    text_lines.append(u"############################\n")
    text_lines.append("Found: %s commands\n" % len(commands_list))
    text_lines.append(u"############################")

    for com in commands_list:
        text_lines.append(u"\n")
        text_lines.append(u"### Command: %s" % com.command)
        text_lines.append(u"# Launched: %s times" % len(com.runs))
        text_lines.append(u"# Average interval: %s mins" % com.get_interval())
        timing_string = get_command_timming(com)
        text_lines.append(u"%s %s   %s %s %s %s" % (
            timing_string[0],
            timing_string[1],
            timing_string[2],
            timing_string[3],
            timing_string[4],
            com.command)
        )

    return text_lines


if __name__ == '__main__':
    main(sys.argv[1:])
    commands_list = []

    # get commands
    list_all = get_all_commands(InputFile, User)
    grouped_all = get_grouped_commands(list_all)

    for item in grouped_all:
        new_command = get_command_item(item, list_all)
        if new_command:
            commands_list.append(new_command)

    # get output
    text_lines = info_messages(commands_list)

    # write output to shell
    for line in text_lines:
        print(line)

    # write output to file (if is defined)
    if OutputFile:
        try:
            f = open(OutputFile, 'w')
            for line in text_lines:
                f.write("%s \n" % line)
            f.close()
        except IOError:
            # error
            print("\nCan't create file %s !" % OutputFile)
